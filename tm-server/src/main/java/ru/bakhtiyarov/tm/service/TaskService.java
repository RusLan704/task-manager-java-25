package ru.bakhtiyarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.repository.ITaskRepository;
import ru.bakhtiyarov.tm.api.service.IManagerFactoryService;
import ru.bakhtiyarov.tm.api.service.ITaskService;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.exception.empty.EmptyDescriptionException;
import ru.bakhtiyarov.tm.exception.empty.EmptyIdException;
import ru.bakhtiyarov.tm.exception.empty.EmptyNameException;
import ru.bakhtiyarov.tm.exception.empty.EmptyUserIdException;
import ru.bakhtiyarov.tm.exception.invalid.InvalidProjectException;
import ru.bakhtiyarov.tm.exception.invalid.InvalidUserException;
import ru.bakhtiyarov.tm.exception.system.IncorrectIndexException;
import ru.bakhtiyarov.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    public final IManagerFactoryService managerFactoryService;

    @NotNull
    public final ServiceLocator serviceLocator;

    @NotNull
    public TaskService(@NotNull IManagerFactoryService managerFactoryService, @NotNull ServiceLocator serviceLocator) {
        super(managerFactoryService);
        this.managerFactoryService = managerFactoryService;
        this.serviceLocator = serviceLocator;
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String userId, @Nullable String projectName, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new InvalidUserException();
        @Nullable Project project = serviceLocator.getProjectService().findOneByName(userId, projectName);
        if (project == null) throw new InvalidProjectException();
        @NotNull Task task = new Task(name);
        task.setUser(user);
        task.setProject(project);
        persist(task);
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String userId, @Nullable String projectName, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new InvalidUserException();
        @Nullable Project project = serviceLocator.getProjectService().findOneByName(userId, projectName);
        if (project == null) throw new InvalidProjectException();
        @NotNull Task task = new Task(name, description);
        task.setUser(user);
        task.setProject(project);
        persist(task);
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final String userId, @Nullable final Task task) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) return;
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ITaskRepository taskRepository = new TaskRepository(em);
        em.getTransaction().begin();
        try {
            taskRepository.remove(userId, task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ITaskRepository taskRepository = new TaskRepository(em);
        em.getTransaction().begin();
        try {
            taskRepository.removeAll(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ITaskRepository taskRepository = new TaskRepository(em);
        em.getTransaction().begin();
        Task task = new Task();
        try {
            task = taskRepository.removeOneByIndex(userId, index);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ITaskRepository taskRepository = new TaskRepository(em);
        return taskRepository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByUserIdAndProjectName(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectName == null || projectName.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = serviceLocator.getProjectService().findOneByName(userId, projectName);
        if (project == null) throw new InvalidProjectException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ITaskRepository taskRepository = new TaskRepository(em);
        return taskRepository.findAllByUserIdAndProjectName(userId, projectName);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ITaskRepository taskRepository = new TaskRepository(em);
        return taskRepository.findAllByUserId();
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ITaskRepository taskRepository = new TaskRepository(em);
        Task task = new Task();
        em.getTransaction().begin();
        try {
            task = taskRepository.removeOneById(userId, id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ITaskRepository taskRepository = new TaskRepository(em);
        Task task = new Task();
        em.getTransaction().begin();
        try {
            task = taskRepository.removeOneByName(userId, name);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return task;
    }

    @Override
    public List<Task> removeAll() {
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ITaskRepository taskRepository = new TaskRepository(em);
        List<Task> tasks = new ArrayList<>();
        em.getTransaction().begin();
        try {
            tasks = taskRepository.removeAll();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return tasks;
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<Task> removeAllByUserIdAndProjectName(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectName == null || projectName.isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = serviceLocator.getProjectService().findOneByName(userId, projectName);
        if (project == null) throw new InvalidProjectException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ITaskRepository taskRepository = new TaskRepository(em);
        List<Task> tasks = new ArrayList<>();
        em.getTransaction().begin();
        try {
            tasks = taskRepository.removeAllByUserIdAndProjectId(userId, projectName);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return tasks;
    }


    @Nullable
    @Override
    @SneakyThrows
    public Task updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return merge(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(@Nullable final String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ITaskRepository taskRepository = new TaskRepository(em);
        return taskRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task updateTaskByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return merge(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ITaskRepository taskRepository = new TaskRepository(em);
        return taskRepository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull EntityManager em = managerFactoryService.getEntityManager();
        @NotNull ITaskRepository taskRepository = new TaskRepository(em);
        return taskRepository.findOneByName(userId, name);
    }

}
