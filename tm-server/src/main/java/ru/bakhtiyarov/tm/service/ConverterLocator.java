package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.api.service.converter.*;
import ru.bakhtiyarov.tm.service.converter.ProjectConverter;
import ru.bakhtiyarov.tm.service.converter.SessionConverter;
import ru.bakhtiyarov.tm.service.converter.TaskConverter;
import ru.bakhtiyarov.tm.service.converter.UserConverter;

public final class ConverterLocator implements IConverterLocator {

    @NotNull
    private final ITaskConverter taskConverter = new TaskConverter();

    @NotNull
    private final IProjectConverter projectConverter = new ProjectConverter();

    @NotNull
    private final IUserConverter userConverter = new UserConverter();

    @NotNull
    private final ISessionConverter sessionConverter = new SessionConverter();


    @NotNull
    @Override
    public ITaskConverter getTaskConverter() {
        return taskConverter;
    }

    @NotNull
    @Override
    public IProjectConverter getProjectConverter() {
        return projectConverter;
    }

    @NotNull
    @Override
    public IUserConverter getUserConverter() {
        return userConverter;
    }

    @NotNull
    @Override
    public ISessionConverter getSessionConverter() {
        return sessionConverter;
    }

}
