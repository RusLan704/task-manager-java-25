package ru.bakhtiyarov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.dto.TaskDTO;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @NotNull
    List<Task> findAllByUserId(@NotNull String userId);

    @NotNull
    List<Task> findAllByUserIdAndProjectName(@NotNull final String userId, @NotNull final String projectId);

    @NotNull
    List<Task> findAllByUserId();

    @Nullable
    Task findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task remove(@NotNull String userId, @NotNull Task project);

    @Nullable
    Task removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task removeOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    List<Task> removeAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

    @NotNull
    List<Task> removeAll(@NotNull String userId);

    @NotNull
    List<Task> removeAll();

}
