package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IPropertyService {

    void init();

    @NotNull
    String getSessionSalt();

    @NotNull
    Integer getSessionCycle();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getServerPort();

    @Nullable
    String getJdbcDriver();

    @Nullable
    String getJdbcUsername();

    @Nullable
    String getJdbcPassword();

    @Nullable
    String getJdbcUrl();
}
