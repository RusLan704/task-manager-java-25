package ru.bakhtiyarov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.endpoint.ITaskEndpoint;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.api.service.converter.IConverterLocator;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.dto.TaskDTO;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint() {
        super(null);
    }

    public TaskEndpoint(final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void createTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "projectName", partName = "projectName") final String projectName,
            @Nullable @WebParam(name = "taskName", partName = "taskName") final String taskName
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), projectName, taskName);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void createTaskByNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "projectName", partName = "projectName") final String projectName,
            @Nullable @WebParam(name = "taskName", partName = "taskName") final String taskName,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), projectName, taskName, description);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public List<TaskDTO> findAllTasksBySession(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable List<Task> tasks = serviceLocator.getTaskService().findAll(sessionDTO.getUserId());
        return tasks
                .stream()
                .map(converterLocator.getTaskConverter()::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void clearAllTasksBySession(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable final Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeAll(sessionDTO.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public TaskDTO findTaskOneById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable final Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable Task task = serviceLocator.getTaskService().findOneById(session.getUserId(), id);
        @Nullable TaskDTO taskDTO = converterLocator.getTaskConverter().toDTO(task);
        return taskDTO;
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public TaskDTO findTaskOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable final Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable Task task = serviceLocator.getTaskService().findOneByIndex(session.getUserId(), index);
        @Nullable TaskDTO taskDTO = converterLocator.getTaskConverter().toDTO(task);
        return taskDTO;
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public TaskDTO findTaskOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable final Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable Task task = serviceLocator.getTaskService().findOneByName(session.getUserId(), name);
        @Nullable TaskDTO taskDTO = converterLocator.getTaskConverter().toDTO(task);
        return taskDTO;
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public TaskDTO removeTaskOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable final Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable Task task = serviceLocator.getTaskService().removeOneByIndex(session.getUserId(), index);
        @Nullable TaskDTO taskDTO = converterLocator.getTaskConverter().toDTO(task);
        return taskDTO;
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public TaskDTO removeTaskOneById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable final Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable Task task = serviceLocator.getTaskService().removeOneById(session.getUserId(), id);
        @Nullable TaskDTO taskDTO = converterLocator.getTaskConverter().toDTO(task);
        return taskDTO;
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public TaskDTO removeTaskOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable final Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable Task task = serviceLocator.getTaskService().removeOneByName(session.getUserId(), name);
        @Nullable TaskDTO taskDTO = converterLocator.getTaskConverter().toDTO(task);
        return taskDTO;
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public TaskDTO updateTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable final Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable Task task = serviceLocator.getTaskService().updateTaskById(session.getUserId(), id, name, description);
        @Nullable TaskDTO taskDTO = converterLocator.getTaskConverter().toDTO(task);
        return taskDTO;
    }

    @Nullable
    @Override
    @WebMethod
    @SneakyThrows
    public TaskDTO updateTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable final Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable Task task = serviceLocator.getTaskService().updateTaskByIndex(session.getUserId(), index, name, description);
        @Nullable TaskDTO taskDTO = converterLocator.getTaskConverter().toDTO(task);
        return taskDTO;
    }

    @Override
    @NotNull
    @WebMethod
    @SneakyThrows
    public List<TaskDTO> findAllByUserIdAndProjectName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "projectName", partName = "projectName") final String projectName
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable final Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable List<Task> tasks = serviceLocator.getTaskService()
                .findAllByUserIdAndProjectName(sessionDTO.getUserId(), projectName);
        return tasks
                .stream()
                .map(converterLocator.getTaskConverter()::toDTO)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public List<TaskDTO> removeAllByUserIdAndProjectName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "projectName", partName = "projectName") final String projectName
    ) {
        @NotNull final IConverterLocator converterLocator = serviceLocator.getConverterLocator();
        @Nullable final Session session = converterLocator.getSessionConverter().toEntity(sessionDTO);
        serviceLocator.getSessionService().validate(session);
        @Nullable List<Task> tasks = serviceLocator.getTaskService()
                .removeAllByUserIdAndProjectName(sessionDTO.getUserId(), projectName);
        return tasks
                .stream()
                .map(converterLocator.getTaskConverter()::toDTO)
                .collect(Collectors.toList());
    }

}
