package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.repository.ITaskRepository;
import ru.bakhtiyarov.tm.entity.Task;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    private final EntityManager em;

    @NotNull
    public TaskRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
        this.em = entityManager;
    }

    @NotNull
    @Override
    public List<Task> removeAll(@NotNull final String userId) {
        @NotNull final List<Task> tasks = findAllByUserId(userId);
        tasks.forEach(em::remove);
        return tasks;
    }

    @Nullable
    @Override
    public Task removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        em.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task remove(@NotNull final String userId, @NotNull final Task task) {
        if (!userId.equals(task.getUser().getId())) return null;
        em.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) return null;
        em.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Task task = findOneByName(userId, name);
        if (task == null) return null;
        em.remove(task);
        return task;
    }

    @Override
    @NotNull
    public List<Task> removeAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId) {
        @NotNull final List<Task> tasks = findAllByUserIdAndProjectName(userId, projectId);
        tasks.forEach(this::remove);
        return tasks;
    }

    @Nullable
    @Override
    public Task findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        List<Task> tasks = em.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
        if (tasks.isEmpty()) return null;
        return tasks.get(index);
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId(@NotNull final String userId) {
        return em.createQuery(
                "SELECT e FROM Task e WHERE e.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByUserIdAndProjectName(@NotNull final String userId, @NotNull final String projectName) {
        return em.createQuery(
                "SELECT e FROM Task e WHERE e.user.id = :userId AND e.project.name = :projectName", Task.class)
                .setParameter("userId", userId)
                .setParameter("projectName", projectName)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        List<Task> tasks = em.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId AND e.id = :id", Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (tasks.isEmpty()) return null;
        return tasks.get(0);
    }

    @Nullable
    @Override
    public Task findOneByName(@NotNull final String userId, @NotNull final String name) {
        List<Task> tasks = em.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId AND e.name = :name", Task.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1)
                .getResultList();
        if (tasks.isEmpty()) return null;
        return tasks.get(0);
    }

    @NotNull
    @Override
    public List<Task> removeAll() {
        @NotNull final List<Task> tasks = findAllByUserId();
        tasks.forEach(this::remove);
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId() {
        return em.createQuery("SELECT e FROM Task e", Task.class).getResultList();
    }

}