package ru.bakhtiyarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.UserDTO;
import ru.bakhtiyarov.tm.endpoint.UserEndpoint;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public final class UserUpdateFirstNameCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.USER_UPDATE_FIRST_NAME;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user first name.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE FIRST NAME]");
        @Nullable final UserEndpoint userEndpoint = endpointLocator.getUserEndpoint();
        @NotNull SessionDTO session = serviceLocator.getSessionService().getSession();
        UserDTO user = userEndpoint.findUserById(session);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        @Nullable final UserDTO userUpdated = userEndpoint.updateUserFirstName(session, firstName);
        if (userUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}