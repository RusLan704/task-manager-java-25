package ru.bakhtiyarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.TaskDTO;
import ru.bakhtiyarov.tm.endpoint.TaskEndpoint;
import ru.bakhtiyarov.tm.util.TerminalUtil;

import java.util.List;

public class TaskListByProjectCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "tasks-list-by-project-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list by project name.";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        System.out.println("ENTER PROJECT NAME:");
        final String projectName = TerminalUtil.nextLine();
        @NotNull final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        @NotNull SessionDTO session = serviceLocator.getSessionService().getSession();
        final List<TaskDTO> tasks = taskEndpoint.findAllByUserIdAndProjectName(session, projectName);
        int index = 1;
        for (@NotNull final TaskDTO task : tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

}