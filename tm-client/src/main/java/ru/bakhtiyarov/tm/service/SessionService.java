package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.service.ISessionService;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.exception.empty.EmptySessionException;
import ru.bakhtiyarov.tm.exception.user.SessionNotFoundException;

public class SessionService implements ISessionService {

    @Nullable
    private SessionDTO session;

    @NotNull
    @Override
    public SessionDTO getSession() {
        if (session == null) throw new SessionNotFoundException();
        return session;
    }

    @Override
    public void setSession(@Nullable final SessionDTO session) {
        if (session == null) throw new EmptySessionException();
        this.session = session;
    }

    @Override
    public void clearSession() {
        session = null;
    }

}
